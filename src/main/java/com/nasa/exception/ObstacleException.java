package com.nasa.exception;

public class ObstacleException extends Exception {

	private static final long serialVersionUID = -3906826060776828570L;
	
	public ObstacleException(String msg) {
		super(msg);
	}
}
